import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import VueAxios from 'vue-axios'

Vue.use(VueAxios, axios)
Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    nameId: null
  },
  getters: {
    getName (state) {
      return state.nameId
    }
  },
  mutations: {
    /**
     * @param {String} nameId The new value for name
     */
    setName (state, nameId) {
      state.nameId = nameId
    }
  },
  actions: {
  },
  modules: {
  }
})
