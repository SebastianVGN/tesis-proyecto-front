import Vue from 'vue'
//import VueRouter from 'vue-router'
import Router from 'vue-router'
//import Home from '../views/Home.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'Home',
      component: () => import(/* webpackChunkName: "home" */ '@/views/Home.vue')
    },
    {
      path: '/about',
      name: 'About',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ '@/views/About.vue')
    },
    {
      path: '/login',
      name: 'Login',
      component: () => import(/* webpackChunkName: "login" */ '@/views/Login.vue')
    },
    {
      path: '/testUser',
      name: 'TestUser',
      component: () => import(/* webpackChunkName: "testUser" */ '@/views/TestUser.vue')
    },
    {
      path: '/rekognition',
      name: 'Rekognition',
      component: () => import(/* webpackChunkName: "rekognition" */ '@/views/Rekognition.vue')
    },
    {
      path: '/evaluator',
      name: 'Evaluator',
      component: () => import(/* webpackChunkName: "evaluator" */ '@/views/Evaluator.vue')
    },
    {
      path: '*',
      component: () => import(/* webpackChunkName: "error" */ '@/views/Error.vue'),
      props: true
    }
  ]
})
